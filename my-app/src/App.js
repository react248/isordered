import List from "./components/List";

function App() {
  const colorList = ["Orange", "Green", "Black", "Yellow"];
  let flag = true;
  return (
    <div>
      <List isOrdered={flag} list={colorList} />
      <List isOrdered={!flag} list={colorList} />
    </div>
  );
}

export default App;
