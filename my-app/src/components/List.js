const List = (props) => {
  if (props.isOrdered)
    return (
      <ol>
        {props.list.map((item) => {
          return <li>{item}</li>;
        })}
      </ol>
    );
  else if (!props.isOrdered) {
    return (
      <ul>
        {props.list.map((item) => {
          return <li>{item}</li>;
        })}
      </ul>
    );
  }
};

export default List;
